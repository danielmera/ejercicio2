
package rombo;

import java.util.Scanner;

/**
 *
 * @author CompuStore
 */
public class Rombo {

    Scanner entrada = new Scanner(System.in);
    int numero;

    public void EntradaDeDato() {
        System.out.println("Ingrese un numero para crear el rombo");
        numero = entrada.nextInt();
    }

    public void EjecucionDelProceso() {
        for (int i = 0; i < numero; i++) {
            for (int j = numero - i; j > 0; j--) {
                System.out.print(" ");

            }
            for (int j = 1; j < i; j++) {
                System.out.print(" *");
            }
            System.out.println(" ");
        }
        for (int i = 0; i <= numero; i++) {
            for (int j = 0; j <= i; j++) {
                System.out.print(" ");

            }
            for (int j = numero - i - 2; j > 0; j--) {
                System.out.print(" *");
            }
            System.out.println(" ");
        }
    }

    public static void main(String[] args) {
        Rombo entrada = new Rombo();
        entrada.EntradaDeDato();
        entrada.EjecucionDelProceso();
    }
}
